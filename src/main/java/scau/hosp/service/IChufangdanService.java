package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Chufangdan;
import scau.hosp.entity.ChufangdanAll;

public interface IChufangdanService extends IBaseService<Chufangdan> {
	
	List<ChufangdanAll> findAllChufangdan();
}
