package scau.hosp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.OrderBy;

import io.swagger.annotations.ApiModelProperty;

public class Binglidan implements Serializable{
	
	private static final long serialVersionUID = 100000L;

	@Id
    private Integer id;

    private Integer patientId;

    private Integer doctorId;

    @OrderBy("desc")
    private Date time;

    @ApiModelProperty("0作废 1有效")
    private Integer status;

    private String content;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}