package scau.hosp.base.web.cors;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class CorsConfigureAdapter  extends WebMvcConfigurerAdapter{

    public void addCorsMappings(CorsRegistry registry){
            registry.addMapping("/user").allowedOrigins("http://localhost:3000");
            registry.addMapping("/login").allowedOrigins("http://localhost:3000");
            registry.addMapping("/logout").allowedOrigins("http://localhost:3000");
            registry.addMapping("/order").allowedOrigins("http://localhost:3000");

    }
}
