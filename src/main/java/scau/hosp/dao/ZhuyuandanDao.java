package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Zhuyuandan;
import scau.hosp.entity.ZhuyuandanAll;

@Repository
public interface ZhuyuandanDao extends IBaseMapper<Zhuyuandan>{
	
	List<ZhuyuandanAll> findAllZhuyuandan();
}