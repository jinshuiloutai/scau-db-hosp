package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Binglidan;
import scau.hosp.entity.BinglidanAll;

public interface IBinglidanService extends IBaseService<Binglidan> {

	List<BinglidanAll> findAllBinglidan();
}
