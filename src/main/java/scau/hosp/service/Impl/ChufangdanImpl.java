package scau.hosp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.ChufangdanDao;
import scau.hosp.entity.Chufangdan;
import scau.hosp.entity.ChufangdanAll;
import scau.hosp.service.IChufangdanService;

@Service
public class ChufangdanImpl extends AbstractBaseService<Chufangdan> implements IChufangdanService{

	@Autowired
	ChufangdanDao ChufangdanDao;
	
	public List<ChufangdanAll> findAllChufangdan() {
		return ChufangdanDao.findAllChufangdan();
	}

}
