package scau.hosp.entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.OrderBy;

import io.swagger.annotations.ApiModelProperty;

public class Department implements Serializable{
	
	private static final long serialVersionUID = 100000L;
	
	@Id
	@OrderBy("desc")
    private Integer id;

	@ApiModelProperty("科室号")
    private String deparmentNumber;

	@ApiModelProperty("科室名")
    private String name;

	@ApiModelProperty("科室地址")
    private String address;

	@ApiModelProperty("科室电话")
    private String phone;
    
    @ApiModelProperty("0作废 1有效")
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeparmentNumber() {
        return deparmentNumber;
    }

    public void setDeparmentNumber(String deparmentNumber) {
        this.deparmentNumber = deparmentNumber == null ? null : deparmentNumber.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}