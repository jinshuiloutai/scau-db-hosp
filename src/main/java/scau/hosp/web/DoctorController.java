package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Doctor;
import scau.hosp.entity.DoctorAll;
import scau.hosp.service.IDoctorService;

@Api(description = "医生控制器")
@RestController
@RequestMapping("/doctor")
public class DoctorController extends BaseController{
	
	@Autowired
	IDoctorService iDoctorService;
	
	@ApiOperation("新添")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", required = true, value = "名字"),
		@ApiImplicitParam(paramType = "query", name = "card", dataType = "String", required = true, value = "工作证"),
		@ApiImplicitParam(paramType = "query", name = "post", dataType = "String", value = "职称"),
		@ApiImplicitParam(paramType = "query", name = "departmentId", dataType = "Integer", required = true, value = "科室id"),
		@ApiImplicitParam(paramType = "query", name = "age", dataType = "Integer", value = "年龄")
	})
	@PostMapping("/add")
	public Map<String, Object> addDoctor(Doctor doctor, @RequestParam String name,
			@RequestParam String card, @RequestParam String departmentId) {
		Map<String, Object> model = new LinkedHashMap<>();
		doctor.setStatus(1);
		if (iDoctorService.add(doctor) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteDoctor(Doctor doctor, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			doctor.setId(deleteId);
			doctor.setStatus(0);
			iDoctorService.updateNotNull(doctor);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("医生信息")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Doctor.class))
	@GetMapping("/show")
	public Map<String, Object> showDoctor(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<DoctorAll> doctors = iDoctorService.findAllDoctor();

		PageInfo<DoctorAll> pageInfo = new PageInfo<DoctorAll>(doctors);
		model.put("content", doctors);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("修改")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "id"),
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", value = "名字"),
		@ApiImplicitParam(paramType = "query", name = "card", dataType = "String", value = "工作证"),
		@ApiImplicitParam(paramType = "query", name = "post", dataType = "String", value = "职称"),
		@ApiImplicitParam(paramType = "query", name = "departmentId", dataType = "Integer", value = "科室id"),
		@ApiImplicitParam(paramType = "query", name = "age", dataType = "Integer", value = "年龄")
	})
	@PostMapping("/update")
	public Map<String, Object> updateDoctor(Doctor doctor, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iDoctorService.updateNotNull(doctor) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}
