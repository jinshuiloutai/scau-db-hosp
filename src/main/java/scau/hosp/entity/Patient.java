package scau.hosp.entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.OrderBy;

import io.swagger.annotations.ApiModelProperty;

public class Patient implements Serializable {
	
	private static final long serialVersionUID = 100000L;
	
	@Id
	@OrderBy("desc")
    private Integer id;

	@ApiModelProperty("患者姓名")
    private String name;

    @ApiModelProperty("性别 0男1女")
    private Integer sex;

    @ApiModelProperty("身份证")
    private String card;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("0作废 1有效")
    private Integer status;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card == null ? null : card.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}