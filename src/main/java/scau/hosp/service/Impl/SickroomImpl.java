package scau.hosp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.SickroomDao;
import scau.hosp.entity.Sickroom;
import scau.hosp.entity.SickroomAll;
import scau.hosp.service.ISickroomService;

@Service
public class SickroomImpl extends AbstractBaseService<Sickroom> implements ISickroomService{

	@Autowired
	SickroomDao sickroomDao;
	
	public List<SickroomAll> findAllSickroom() {
		return sickroomDao.findAllSickroom();
	}

}
