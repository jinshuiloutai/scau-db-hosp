package scau.hosp.dao;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Patient;

@Repository
public interface PatientDao extends IBaseMapper<Patient>{
	
	Patient findPatientById(Integer id);
}