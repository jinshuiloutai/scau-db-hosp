package scau.hosp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.BinglidanDao;
import scau.hosp.entity.Binglidan;
import scau.hosp.entity.BinglidanAll;
import scau.hosp.service.IBinglidanService;

@Service
public class BinglidanImpl extends AbstractBaseService<Binglidan> implements IBinglidanService{

	@Autowired
	BinglidanDao binglidanDao;

	public List<BinglidanAll> findAllBinglidan() {
		return binglidanDao.findAllBinglidan();
	}
}
