package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Patient;
import scau.hosp.service.IPatientService;

@Api(description = "病人控制器")
@RestController
@RequestMapping("/patient")
public class PatientController extends BaseController{
	
	@Autowired
	IPatientService iPatientService;
	
	@ApiOperation("添加")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", required = true, value = "姓名"),
		@ApiImplicitParam(paramType = "query", name = "sex", dataType = "Integer", value = "性别 0男1女"),
		@ApiImplicitParam(paramType = "query", name = "card", dataType = "String", value = "身份证"),
		@ApiImplicitParam(paramType = "query", name = "phone", dataType = "String", value = "联系方式")
	})
	@PostMapping("/add")
	public Map<String, Object> addPatient(Patient patient, @RequestParam String name){
		Map<String, Object> model = new LinkedHashMap<>();
		patient.setStatus(1);
		if (iPatientService.add(patient) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
    @ApiOperation("批量删除病人")
    @PostMapping("/delete")
    public Map<String, Object> deletePatient(Patient patient , @RequestParam("id") List<Integer> idList) {
        Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			patient.setId(deleteId);
			patient.setStatus(0);
			iPatientService.updateNotNull(patient);
		}
        sendCode(model, GlobalCode.OPERATION_SUCCESS);
        return model;
    }

    @ApiOperation("姓名查询")
    @ApiResponses(@ApiResponse(code = 200,message = "详情",response = Patient.class))
    @GetMapping("getByName")
    public Map<String, Object> getPatientByName(@RequestParam String name){
		Map<String, Object> model = new LinkedHashMap<>();
		List<Patient> patients = iPatientService.listEntityByCondition(name, "name");
		model.put("content", patients);
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
    
	@ApiOperation("病人信息")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Patient.class))
	@GetMapping("show")
	public Map<String, Object> showPatint(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<Patient> patients = iPatientService.findSelectPropertyWhereAnd("status", 1);
		PageInfo<Patient> pageInfo = new PageInfo<Patient>(patients);
		model.put("content", patients);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("修改信息")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "id"),
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", value = "姓名"),
		@ApiImplicitParam(paramType = "query", name = "sex", dataType = "Integer", value = "性别 0男1女1"),
		@ApiImplicitParam(paramType = "query", name = "card", dataType = "String", value = "身份证"),
		@ApiImplicitParam(paramType = "query", name = "phone", dataType = "String", value = "联系方式")
	})
	@PostMapping("/update")
	public Map<String, Object> updatePatient(Patient patient, @RequestParam Integer id){
 		Map<String, Object> model = new LinkedHashMap<>();
		if (iPatientService.updateNotNull(patient) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}
