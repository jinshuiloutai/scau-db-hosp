package scau.hosp.dictionary;

import scau.hosp.base.common.IBaseCode;

public enum ActivitiesCode implements IBaseCode {
    ;

    private int code;
    private String message;
    ActivitiesCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
