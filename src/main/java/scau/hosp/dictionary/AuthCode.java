package scau.hosp.dictionary;

public enum AuthCode {
    TOO_MANY_VISIT(301, "访问次数过多"),
    ;
    private int code;
    private String message;
    AuthCode(int statusCode, String statusMsg) {
        this.code = statusCode;
        this.message = statusMsg;
    }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
