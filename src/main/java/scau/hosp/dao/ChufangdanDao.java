package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Chufangdan;
import scau.hosp.entity.ChufangdanAll;

@Repository
public interface ChufangdanDao extends IBaseMapper<Chufangdan>{
	
	List<ChufangdanAll> findAllChufangdan();
}