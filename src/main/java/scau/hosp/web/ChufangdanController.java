package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Chufangdan;
import scau.hosp.entity.ChufangdanAll;
import scau.hosp.service.IChufangdanService;

@Api(description = "处方单控制器")
@RestController
@RequestMapping("/chufangdan")
public class ChufangdanController extends BaseController {
 
	@Autowired
	IChufangdanService	iChufangdanService;
	
	@ApiOperation("生成")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "patientId", dataType = "Integer", required = true, value = "病人id"),
		@ApiImplicitParam(paramType = "query", name = "doctorId", dataType = "Integer", required = true, value = "医生id"),
		@ApiImplicitParam(paramType = "query", name = "content", dataType = "String", required = true, value = "病历内容"),
		@ApiImplicitParam(paramType = "query", name = "cost", dataType = "Double", required = true, value = "费用")
	})
	@PostMapping("/add")
	public Map<String, Object> addChufangdan(Chufangdan chufangdan, @RequestParam Integer patientId,
			@RequestParam Integer doctorId, @RequestParam String content, @RequestParam Double cost) {
		Map<String, Object> model = new LinkedHashMap<>();
		chufangdan.setStatus(1);
		if (iChufangdanService.add(chufangdan) == 1) {
			sendCode(model, GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model, GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteChufangdan(Chufangdan chufangdan, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			chufangdan.setId(deleteId);
			chufangdan.setStatus(0);
			iChufangdanService.updateNotNull(chufangdan);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("全部有效处方单")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Chufangdan.class))
	@GetMapping("/show")
	public Map<String, Object> showBinglidan(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<ChufangdanAll> chufangdanAlls = iChufangdanService.findAllChufangdan();
		PageInfo<ChufangdanAll> pageInfo = new PageInfo<ChufangdanAll>(chufangdanAlls);
		model.put("content", chufangdanAlls);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("修改处方单")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "处方单id"),
		@ApiImplicitParam(paramType = "query", name = "content", dataType = "String", value = "内容"),
		@ApiImplicitParam(paramType = "query", name = "cost", dataType = "Double", value = "花费")
	})
	@PostMapping("/update")
	public Map<String, Object> updatechufangdan(Chufangdan chufangdan, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iChufangdanService.updateNotNull(chufangdan) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}