package scau.hosp.dao;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Department;

@Repository
public interface DepartmentDao extends IBaseMapper<Department>{
	
	
}