package scau.hosp.entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.OrderBy;

import io.swagger.annotations.ApiModelProperty;

public class SickroomAll implements Serializable{
	
	private static final long serialVersionUID = 100000L;
	
	@Id
    private Integer id;

    private String roomNumber;

    private String bedNumber;

    private Integer bedStatus;

    @OrderBy("desc")
    private Integer departmentId;
    
    private Department department;
    
    @ApiModelProperty("0作废 1有效")
    private Integer status;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber == null ? null : roomNumber.trim();
    }

    public String getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(String bedNumber) {
        this.bedNumber = bedNumber == null ? null : bedNumber.trim();
    }

    public Integer getBedStatus() {
        return bedStatus;
    }

    public void setBedStatus(Integer bedStatus) {
        this.bedStatus = bedStatus;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
    
    public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}